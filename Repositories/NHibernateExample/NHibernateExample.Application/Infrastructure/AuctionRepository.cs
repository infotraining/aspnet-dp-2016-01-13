﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using NHibernateExample.Application.Model.Auction;

namespace NHibernateExample.Application.Infrastructure
{
    public class AuctionRepository : IAuctionRepository
    {
        private readonly ISession _session;

        public AuctionRepository(ISession session)
        {
            _session = session;
        }

        public void Add(Auction auction)
        {
            _session.Save(auction);
        }

        public Auction FindBy(Guid id)
        {
            return _session.Get<Auction>(id);
        }
    }
}
