﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using NHibernateExample.Application.Model.BidHistory;

namespace NHibernateExample.Application.Infrastructure
{
    public class BidHistoryRepository : IBidHistoryRepository
    {
        private readonly ISession _session;

        public BidHistoryRepository(ISession session)
        {
            _session = session;
        }

        public int NoOfBidsFor(Guid auctionId)
        {
            var sql = string.Format("SELECT Count(*) FROM BidHistory WHERE AuctionId = '{0}'", auctionId);
            var query = _session.CreateSQLQuery(sql);
            var result = query.UniqueResult();

            return Convert.ToInt32(result);
        }

        public void Add(Bid bid)
        {
            _session.Save(bid);
        }
    }
}
