﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using NHibernate.Transform;
using NHibernateExample.Application.Infrastructure;
using NHibernateExample.Application.Model.BidHistory;

namespace NHibernateExample.Application.Application.Queries
{
    public class AuctionStatusQuery
    {
        private ISession _session;
        private IBidHistoryRepository _bidHistory;
        private IClock _clock;

        public AuctionStatusQuery(ISession session, IBidHistoryRepository bidHistory, IClock clock)
        {
            _session = session;
            _bidHistory = bidHistory;
            _clock = clock;
        }

        public AuctionStatus AuctionStatus(Guid auctionId)
        {
            var status = _session.CreateSQLQuery(
                string.Format("SELECT Id, CurrentPrice, BidderMemberId as WinningBidderId, " +
                              "AuctionEnds from Auctions WHERE Id = '{0}'", auctionId))
                .SetResultTransformer(Transformers.AliasToBean<AuctionStatus>())
                .UniqueResult<AuctionStatus>();

            status.TimeRemaining = TimeRemaining(status.AuctionEnds);
            status.NumberOfBids = _bidHistory.NoOfBidsFor(auctionId);

            return status;
        }

        private TimeSpan TimeRemaining(DateTime auctionEnds)
        {
            if (_clock.Time() < auctionEnds)
                return auctionEnds.Subtract(_clock.Time());
            else
            {
                return new TimeSpan();
            }
        }
    }

}
