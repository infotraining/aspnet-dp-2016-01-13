﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using NHibernate.Transform;

namespace NHibernateExample.Application.Application.Queries
{
    public class BidHistoryQuery
    {
        private readonly ISession _session;
        public BidHistoryQuery(ISession session)
        {
            _session = session;
        }

        public IEnumerable<BidInformation> BidHistoryFor(Guid auctionId)
        {
            var status = _session
                            .CreateSQLQuery(
                                String.Format(
                                    "SELECT [BidderId] as Bidder,[Bid] as AmountBid ,TimeOfBid " +
                                    "FROM [BidHistory] " +
                                    "Where AuctionId = '{0}' " +
                                    "Order By Bid Desc, TimeOfBid Asc", auctionId))
                            .SetResultTransformer(Transformers.AliasToBean<BidInformation>());

            return status.List<BidInformation>();
        }
    }
}
