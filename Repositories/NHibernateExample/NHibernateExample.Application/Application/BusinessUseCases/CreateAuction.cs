﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernateExample.Application.Model.Auction;
using NHibernate;

namespace NHibernateExample.Application.Application.BusinessUseCases
{
    public class CreateAuction
    {
        private IAuctionRepository _auctionRepository;
        private ISession _unitOfWork;

        public CreateAuction(IAuctionRepository auctionRepository, ISession unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _auctionRepository = auctionRepository;
        }

        public Guid Create(NewAuctionRequest newAuctionRequest)
        {
            var auctionId = Guid.NewGuid();

            var startingPrice = new Money(newAuctionRequest.StartingPrice);

            using (ITransaction transaction = _unitOfWork.BeginTransaction())
            {
                _auctionRepository.Add(new Auction(auctionId, startingPrice, newAuctionRequest.EndsAt));

                transaction.Commit();
            }

            return auctionId;
        }
    }
}
