﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using NHibernateExample.Application.Infrastructure;
using NHibernateExample.Application.Model.Auction;
using NHibernateExample.Application.Model.BidHistory;

namespace NHibernateExample.Application.Application.BusinessUseCases
{
    public class BidOnAuction
    {
        private IAuctionRepository _auctionRepository;
        private IBidHistoryRepository _bidHistoryRepository;
        private ISession _unitOfWork;
        private IClock _clock;

        public BidOnAuction(IAuctionRepository auctionRepository, IBidHistoryRepository bidHistoryRepository, ISession unitOfWork, IClock clock)
        {
            _auctionRepository = auctionRepository;
            _bidHistoryRepository = bidHistoryRepository;
            _unitOfWork = unitOfWork;
            _clock = clock;
        }

        public void Bid(Guid auctionId, Guid memberId, decimal amount)
        {
            try
            {
                using (ITransaction transaction = _unitOfWork.BeginTransaction())
                {
                    using (DomainEvents.Register<OutBid>(OutBid()))
                    using (DomainEvents.Register<BidPlaced>(BidPlaced()))
                    {
                        var auction = _auctionRepository.FindBy(auctionId);
                        var bidAmount = new Money(amount);
                        auction.PlaceBidFor(new Offer(memberId, bidAmount, _clock.Time()), _clock.Time());

                        transaction.Commit();
                    }
                }

            }
            catch (StaleObjectStateException ex)
            {
                _unitOfWork.Clear();
                Bid(auctionId, memberId, amount);
            }
        }

        private Action<BidPlaced> BidPlaced()
        {
            return (BidPlaced e) =>
            {
                var bidEvent = new Bid(e.AuctionId, e.Bidder, e.AmountBid, e.TimeOfMemberBid);

                _bidHistoryRepository.Add(bidEvent);
            };
        }

        private Action<OutBid> OutBid()
        {
            return (OutBid e) =>
            {
                // E-mail to customer to say that he has been outbid
            };
        }
    }
}
