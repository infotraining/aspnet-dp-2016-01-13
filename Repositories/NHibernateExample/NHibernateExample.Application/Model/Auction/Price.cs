﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernateExample.Application.Infrastructure;

namespace NHibernateExample.Application.Model.Auction
{
    public class Price : ValueObject<Price>
    {
        private Price()
        {           
        }

        public Price(Money amount)
        {
            if (amount == null)
                throw new ArgumentNullException("Amount cannot be null");

            Amount = amount;
        }

        public Money Amount { get; private set; }

        public Money BidIncrement()
        {
            if (Amount.IsGreaterThanOrEqualTo(new Money(0.01M)) && Amount.IsLessThanOrEqualTo(new Money(0.99M)))
                return Amount.Add(new Money(0.05M));

            if (Amount.IsGreaterThanOrEqualTo(new Money(1.0M)) && Amount.IsLessThanOrEqualTo(new Money(4.99M)))
                return Amount.Add(new Money(0.2M));

            if (Amount.IsGreaterThanOrEqualTo(new Money(5.0M)) && Amount.IsLessThanOrEqualTo(new Money(14.99M)))
                return Amount.Add(new Money(0.5M));

            return Amount.Add(new Money(1.00M));
        }

        public bool CanBeExceededBy(Money offer)
        {
            return offer.IsGreaterThanOrEqualTo(BidIncrement());
        }

        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<object>() { Amount };
        }
    }
}
