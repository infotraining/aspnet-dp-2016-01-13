﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NHibernateExample.Application.Model.Auction
{
    public interface IAuctionRepository
    {
        void Add(Auction auction);

        Auction FindBy(Guid id);
    }
}
