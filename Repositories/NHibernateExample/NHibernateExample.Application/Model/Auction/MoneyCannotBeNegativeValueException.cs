using System;

namespace NHibernateExample.Application.Model.Auction
{
    public class MoneyCannotBeNegativeValueException : Exception
    {
    }
}