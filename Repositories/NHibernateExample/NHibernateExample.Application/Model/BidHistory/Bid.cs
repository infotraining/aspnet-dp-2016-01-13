﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernateExample.Application.Infrastructure;
using NHibernateExample.Application.Model.Auction;

namespace NHibernateExample.Application.Model.BidHistory
{
    public class Bid : ValueObject<Bid>
    {
        private Guid Id { get; set; }

        private Bid()
        {            
        }

        public Bid(Guid auctionId, Guid bidderId, Money amountBid, DateTime timeOfBid)
        {
            if (auctionId == Guid.Empty)
                throw new ArgumentNullException("Auction Id cannot be null");
            if (bidderId == Guid.Empty)
                throw new ArgumentNullException("Bidder Id cannot be null");
            if (amountBid == null)
                throw new ArgumentNullException("AmountBid cannot be null");
            if (timeOfBid == DateTime.MinValue)
                throw new ArgumentNullException("TimeOfBid must have a value");
            AuctionId = auctionId;
            Bidder = bidderId;
            AmountBid = amountBid;
            TimeOfBid = timeOfBid;
        }

        public DateTime TimeOfBid { get; private set; }

        public Money AmountBid { get; private set; }

        public Guid Bidder { get; private set; }

        public Guid AuctionId { get; private set; }

        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<object>() { Bidder, AuctionId, TimeOfBid, AmountBid };
        }
    }
}
