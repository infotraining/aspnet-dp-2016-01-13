﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Lifetime;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using NHibernate.Cfg;
using NHibernateExample.Application.Infrastructure;
using NHibernateExample.Application.Model.Auction;
using NHibernateExample.Application.Model.BidHistory;
using SimpleInjector;

namespace NHibernateExample.Application
{
    public static class Bootstraper
    {
        public static void Startup(Container container)
        {
            Configuration config = new Configuration();
            config.Configure();
            config.AddAssembly("NHibernateExample.Application");

            var sessionFactory = config.BuildSessionFactory();


            container.Register<IAuctionRepository, AuctionRepository>();
            container.Register<IBidHistoryRepository, BidHistoryRepository>();
            container.Register<IClock, SystemClock>();
            container.RegisterSingleton<ISessionFactory>(sessionFactory);
            container.Register<ISession>(() =>
            {
                var factory = container.GetInstance<ISessionFactory>();
                return factory.OpenSession();
            }, Lifestyle.Singleton);
        }
    }
}
