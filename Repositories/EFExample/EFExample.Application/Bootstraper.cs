﻿using EFExample.Application.Infrastructure;
using EFExample.Application.Model.Auction;
using EFExample.Application.Model.BidHistory;
using SimpleInjector;

namespace EFExample.Application
{
    public static class Bootstraper
    {
        public static void Startup(Container container)
        {
            container.Register<IAuctionRepository, AuctionRepository>();
            container.Register<IBidHistoryRepository, BidHistoryRepository>();
            container.Register<IClock, SystemClock>();
            container.Register<AuctionDbContext, AuctionDbContext>(Lifestyle.Singleton);
        }
    }
}