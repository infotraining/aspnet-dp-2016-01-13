﻿using System;
using System.Collections.Generic;
using System.Linq;
using EFExample.Application.Model.BidHistory;

namespace EFExample.Application.Application.Queries
{
    public class BidHistoryQuery
    {
        private readonly IBidHistoryRepository _bidHistoryRepository;

        public BidHistoryQuery(IBidHistoryRepository bidHistoryRepository)
        {
            _bidHistoryRepository = bidHistoryRepository;
        }

        public IEnumerable<BidInformation> BidHistoryFor(Guid auctionId)
        {
            var bidHistory = _bidHistoryRepository.FindBy(auctionId);
            return Convert(bidHistory.ShowAllBids());
        }

        public IEnumerable<BidInformation> Convert(IEnumerable<Bid> bids)
        {
            return bids.Select(bid => new BidInformation()
            {
                Bidder = bid.Bidder, AmountBid = bid.AmountBid.GetSnapshot().Value, TimeOfBid = bid.TimeOfBid
            }).ToList();
        }
    }
}