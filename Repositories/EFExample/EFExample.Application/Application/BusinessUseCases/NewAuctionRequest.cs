﻿using System;

namespace EFExample.Application.Application.BusinessUseCases
{

    // DTO object
    public class NewAuctionRequest
    {
        public decimal StartingPrice { get; set; }
        public DateTime EndsAt { get; set; }
    }
}
