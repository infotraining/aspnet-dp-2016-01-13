﻿using System;
using EFExample.Application.Infrastructure;
using EFExample.Application.Model.Auction;
using EFExample.Application.Model.BidHistory;

namespace EFExample.Application.Application.BusinessUseCases
{
    public class CreateAuction
    {
        private IAuctionRepository _auctionRepository;
        private AuctionDbContext _unitOfWork;

        public CreateAuction(IAuctionRepository auctionRepository, AuctionDbContext unitOfWork)
        {
            _auctionRepository = auctionRepository;
            _unitOfWork = unitOfWork;
        }

        public Guid Create(NewAuctionRequest newAuctionRequest)
        {
            var auctionId = Guid.NewGuid();
            var startingPrice = new Money(newAuctionRequest.StartingPrice);

            _auctionRepository.Add(new Auction(auctionId, startingPrice, newAuctionRequest.EndsAt));

            _unitOfWork.SaveChanges();

            return auctionId;
        }
    }
}