﻿using System;
using System.Data.Entity.Infrastructure;
using EFExample.Application.Infrastructure;
using EFExample.Application.Model.Auction;
using EFExample.Application.Model.BidHistory;

namespace EFExample.Application.Application.BusinessUseCases
{
    public class BidOnAuction
    {
        private IAuctionRepository _auctionRepository;
        private IBidHistoryRepository _bidHistoryRepository;
        private AuctionDbContext _unitOfWork;
        private IClock _clock;


        public BidOnAuction(IAuctionRepository auctionRepository, IBidHistoryRepository bidHistoryRepository, AuctionDbContext unitOfWork, IClock clock)
        {
            _auctionRepository = auctionRepository;
            _bidHistoryRepository = bidHistoryRepository;
            _unitOfWork = unitOfWork;
            _clock = clock;
        }

        public void Bid(Guid auctionId, Guid memberId, decimal amount)
        {
            try
            {
                using (DomainEvents.Register(OutBid()))
                using (DomainEvents.Register(BidPlaced()))
                {
                    var auction = _auctionRepository.FindBy(auctionId);
                    var bidAmount = new Money(amount);

                    auction.PlaceBidFor(new Offer(memberId, bidAmount, _clock.Time()), _clock.Time());

                    _auctionRepository.Save(auction);
                }

                _unitOfWork.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                _unitOfWork.Clear();

                Bid(auctionId, memberId, amount);
            }
        }

        private Action<BidPlaced> BidPlaced()
        {
            return (BidPlaced e) =>
            {
                var bidEvent = new Bid(e.AuctionId, e.Bidder, e.AmountBid, e.TimeOfBid);

                _bidHistoryRepository.Add(bidEvent);
            };
        }

        private Action<OutBid> OutBid()
        {
            return (OutBid e) =>
            {
                // E-mail customer to say that he has been outbid
            };
        }
    }
}