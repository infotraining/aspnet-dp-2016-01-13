﻿using System;
using EFExample.Application.Infrastructure.DataModel;
using EFExample.Application.Model.Auction;

namespace EFExample.Application.Infrastructure
{
    public class AuctionRepository : IAuctionRepository
    {
        private readonly AuctionDbContext _auctionDbContext;

        public AuctionRepository(AuctionDbContext auctionDbContext)
        {
            _auctionDbContext = auctionDbContext;
        }

        public void Add(Auction auction)
        {
            var auctionDTO = new AuctionDTO();

            Map(auctionDTO, auction.GetSnapshot());

            _auctionDbContext.Auctions.Add(auctionDTO);
        }

        public void Save(Auction auction)
        {
            var auctionDTO = _auctionDbContext.Auctions.Find(auction.Id);

            Map(auctionDTO, auction.GetSnapshot());
        }

        public Auction FindBy(Guid id)
        {
            var auctionDTO = _auctionDbContext.Auctions.Find(id);

            var auctionSnapshot = new AuctionSnapshot();
            auctionSnapshot.Id = auctionDTO.Id;
            auctionSnapshot.EndsAt = auctionDTO.AuctionEnds;
            auctionSnapshot.StartingPrice = auctionDTO.StartingPrice;
            auctionSnapshot.Version = auctionDTO.Version;
            if (auctionDTO.BidderMemberId.HasValue)
            {
                var bidSnapshot = new WinningBidSnapshot();
                bidSnapshot.BiddersMaximumBid = auctionDTO.MaximumBid.Value;
                bidSnapshot.CurrentPrice = auctionDTO.CurrentPrice.Value;
                bidSnapshot.BiddersId = auctionDTO.BidderMemberId.Value;
                bidSnapshot.TimeOfBid = auctionDTO.TimeOfBid.Value;
                auctionSnapshot.WinningBid = bidSnapshot;
            }
            return Auction.CreateFrom(auctionSnapshot);
        }

        public void Map(AuctionDTO auctionDTO, AuctionSnapshot snapshot)
        {
            auctionDTO.Id = snapshot.Id;
            auctionDTO.StartingPrice = snapshot.StartingPrice;
            auctionDTO.AuctionEnds = snapshot.EndsAt;
            auctionDTO.Version = snapshot.Version;
            if (snapshot.WinningBid != null)
            {
                auctionDTO.BidderMemberId = snapshot.WinningBid.BiddersId;
                auctionDTO.CurrentPrice = snapshot.WinningBid.CurrentPrice;
                auctionDTO.MaximumBid = snapshot.WinningBid.BiddersMaximumBid;
                auctionDTO.TimeOfBid = snapshot.WinningBid.TimeOfBid;
            }
        }
    }
}