﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using EFExample.Application.Infrastructure.DataModel;
using EFExample.Application.Infrastructure.Mapping;

namespace EFExample.Application.Infrastructure
{
    public class AuctionDbContext : DbContext
    {
        static AuctionDbContext()
        {
            Database.SetInitializer<AuctionDbContext>(null);
        }

        public AuctionDbContext() : base("Name=AuctionDbContext")
        {            
        }

        public DbSet<AuctionDTO> Auctions { get; set; }
        public DbSet<BidDTO> Bids { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new AuctionMap());
            modelBuilder.Configurations.Add(new BidMap());
        }

        public void Clear()
        {
            var context = ((IObjectContextAdapter) this).ObjectContext;

            var addedObjects = context.ObjectStateManager.GetObjectStateEntries(EntityState.Added);

            foreach(var objectStateEntry in addedObjects)
                context.Detach(objectStateEntry.Entity);

            var modifiedObjects = context.ObjectStateManager.GetObjectStateEntries(EntityState.Modified);

            foreach (var objectStateEntry in modifiedObjects)
            {
                context.Detach(objectStateEntry.Entity);
            }
        }
    }
}