﻿using System.Data.Entity.ModelConfiguration;
using EFExample.Application.Infrastructure.DataModel;

namespace EFExample.Application.Infrastructure.Mapping
{
    public class BidMap : EntityTypeConfiguration<BidDTO>
    {
        public BidMap()
        {
            this.HasKey(t => t.Id);
            this.ToTable("BidHistory");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.AuctionId).HasColumnName("AuctionId");
            this.Property(t => t.BidderId).HasColumnName("BidderId");
            this.Property(t => t.Bid).HasColumnName("Bid");
            this.Property(t => t.TimeOfBid).HasColumnName("TimeOfBid");
        }
    }
}