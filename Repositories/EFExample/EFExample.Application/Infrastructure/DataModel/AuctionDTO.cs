﻿using System;

namespace EFExample.Application.Infrastructure.DataModel
{
    public partial class AuctionDTO
    {
        public Guid Id { get; set; }
        public decimal StartingPrice { get; set; }
        public DateTime AuctionEnds { get; set; }

        public Guid? BidderMemberId { get; set; }
        public DateTime? TimeOfBid { get; set; }
        public decimal? MaximumBid { get; set; }
        public decimal? CurrentPrice { get; set; }

        public int Version { get; set; }
    }
}