using System;

namespace EFExample.Application.Infrastructure
{
    class SystemClock : IClock
    {
        public DateTime Time()
        {
            return DateTime.Now;
        }
    }
}