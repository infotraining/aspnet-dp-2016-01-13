﻿using System;
using System.Collections.Generic;
using System.Linq;
using EFExample.Application.Infrastructure.DataModel;
using EFExample.Application.Model.Auction;
using EFExample.Application.Model.BidHistory;

namespace EFExample.Application.Infrastructure
{
    public class BidHistoryRepository : IBidHistoryRepository
    {
        private readonly AuctionDbContext _auctionDbContext;

        public BidHistoryRepository(AuctionDbContext auctionDbContext)
        {
            _auctionDbContext = auctionDbContext;
        }

        public int NoOfBidsFor(Guid auctionId)
        {
            return _auctionDbContext.Bids.Count(b => b.AuctionId == auctionId);
        }

        public void Add(Bid bid)
        {
            var bidDTO = new BidDTO();
            bidDTO.AuctionId = bid.AuctionId;
            bidDTO.Bid = bid.AmountBid.GetSnapshot().Value;
            bidDTO.BidderId = bid.Bidder;
            bidDTO.TimeOfBid = bid.TimeOfBid;
            bidDTO.Id = Guid.NewGuid();
            _auctionDbContext.Bids.Add(bidDTO);
        }

        public BidHistory FindBy(Guid auctionId)
        {
            var bidDTOs = _auctionDbContext.Bids.Where<BidDTO>(x => x.AuctionId == auctionId).ToList();
            var bids = new List<Bid>();

            foreach (var bidDTO in bidDTOs)
            {
                bids.Add(new Bid(bidDTO.AuctionId, bidDTO.BidderId,
                new Money(bidDTO.Bid), bidDTO.TimeOfBid));
            }

            return new BidHistory(bids);
        }
    }
}