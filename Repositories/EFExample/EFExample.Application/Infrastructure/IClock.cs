﻿using System;

namespace EFExample.Application.Infrastructure
{
    public interface IClock
    {
        DateTime Time();
    }
}
