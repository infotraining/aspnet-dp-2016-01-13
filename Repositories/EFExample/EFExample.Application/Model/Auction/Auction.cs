﻿using System;
using EFExample.Application.Infrastructure;

namespace EFExample.Application.Model.Auction
{
    public class Auction : Entity<Guid>
    {
        private Auction() : base(Guid.Empty)
        {            
        }

        public Auction(Guid id, Money startingPrice, DateTime endsAt) : base(id)
        {
            if (startingPrice == null)
                throw new ArgumentNullException("Starting Price cannot be null");

            if (endsAt == DateTime.MinValue)
                throw new ArgumentNullException("EndsAt must have a value");

            StartingPrice = startingPrice;
            EndsAt = endsAt;
        }

        private Auction(AuctionSnapshot snapshot) : base(snapshot.Id)
        {
            this.StartingPrice = new Money(snapshot.StartingPrice);
            this.EndsAt = snapshot.EndsAt;
            this.Version = snapshot.Version;

            if (snapshot.WinningBid != null)
                this.WinningBid = WinningBid.CreateFrom(snapshot.WinningBid);
        }

        public static Auction CreateFrom(AuctionSnapshot snapshot)
        {
            return new Auction(snapshot);
        }

        public AuctionSnapshot GetSnapshot()
        {
            var snapshot = new AuctionSnapshot();
            snapshot.Id = this.Id;
            snapshot.StartingPrice = this.StartingPrice.GetSnapshot().Value;
            snapshot.EndsAt = this.EndsAt;
            snapshot.Version = this.Version;
            if (HasACurrentBid())
                snapshot.WinningBid = WinningBid.GetSnapshot();
            return snapshot;
        }

        private bool HasACurrentBid()
        {
            return WinningBid != null;
        }

        public DateTime EndsAt { get; set; }

        public Money StartingPrice { get; set; }

        private bool StillInProgress(DateTime curreTime)
        {
            return (EndsAt > curreTime);
        }

        public void PlaceBidFor(Offer offer, DateTime currentTime)
        {
            if (StillInProgress(currentTime))
            {
                if (FirstOffer())
                    PlaceBidForTheFirst(offer);
                else if (BidderIsIncreasingMaximumBidToNew(offer))
                {
                    WinningBid = WinningBid.RaiseMaximumBidTo(offer.MaximumBid);

                }
                else if (WinningBid.CanBeExceededBy(offer.MaximumBid))
                {
                    var newBids = new AutomaticBidder().GenerateNextSequenceOfBidsAfter(offer, WinningBid);

                    foreach (var bid in newBids)
                        Place(bid);
                }

            }
        }

        private void Place(WinningBid newBid)
        {
            if (!FirstOffer() && WinningBid.WasMadeBy(newBid.Bidder))
                DomainEvents.Raise(new OutBid(Id, WinningBid.Bidder));

            WinningBid = newBid;

            DomainEvents.Raise(new BidPlaced(Id, newBid.Bidder, newBid.CurrentAuctionPrice.Amount, newBid.TimeOfBid));
        }

        private bool FirstOffer()
        {
            return WinningBid == null;
        }

        private void PlaceBidForTheFirst(Offer offer)
        {
            if (offer.MaximumBid.IsGreaterThanOrEqualTo(StartingPrice))
                Place(new WinningBid(offer.Bidder, offer.MaximumBid, StartingPrice, offer.TimeOfOffer));
        }

        private bool BidderIsIncreasingMaximumBidToNew(Offer offer)
        {
            return WinningBid.WasMadeBy(offer.Bidder) && offer.MaximumBid.IsGreaterThan(WinningBid.MaximumBid);
        }

        public WinningBid WinningBid { get; set; }
    }
}
