﻿using System;
using System.Collections.Generic;
using EFExample.Application.Infrastructure;

namespace EFExample.Application.Model.Auction
{
    public class Offer : ValueObject<Offer>
    {
        public Offer(Guid bidder, Money maximumBid, DateTime timeOfOffer)
        {
            if (bidder == Guid.Empty)
                throw new ArgumentNullException("Bidder cannot be null");
            
            if (maximumBid == null)
                throw new ArgumentNullException("MaximumBid cannot be null");

            if (timeOfOffer == DateTime.MinValue)
                throw new ArgumentNullException("Time of offer must have value");

            Bidder = bidder;
            MaximumBid = maximumBid;
            TimeOfOffer = timeOfOffer;
        }

        public Guid Bidder { get; private set; }
        public Money MaximumBid { get; private set; }
        public DateTime TimeOfOffer { get; private set; }

        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<object>() {Bidder, MaximumBid, TimeOfOffer};
        }
    }
}
