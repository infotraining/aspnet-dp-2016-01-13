using System;

namespace EFExample.Application.Model.Auction
{
    public class MoneyCannotBeNegativeValueException : Exception
    {
    }
}