﻿using System;

namespace EFExample.Application.Model.Auction
{
    public interface IAuctionRepository
    {
        void Add(Auction auction);
        void Save(Auction auction);

        Auction FindBy(Guid id);
    }
}
