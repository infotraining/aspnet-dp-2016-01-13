﻿using System;

namespace EFExample.Application.Model.BidHistory
{
    public interface IBidHistoryRepository
    {
        int NoOfBidsFor(Guid auctionId);

        void Add(Bid bid);

        BidHistory FindBy(Guid auctionId);
    }
}
