﻿namespace DependencyInjection.Dependencies
{
    public interface ILoggingComponent
    {
        void LogMessage(string message);
    }

    public class LoggingComponent : ILoggingComponent
    {
        private ILoggingDataSink _sink;
        private string _logName;

        public LoggingComponent(ILoggingDataSink sink, string logName)
        {
            _sink = sink;
            _logName = logName;
        }

        public void LogMessage(string message)
        {
            _sink.Write(_logName + " >>> " + message);
        }
    }
}
