﻿using System.Configuration;
using DependencyInjection.Dependencies;
using SimpleInjector;

namespace DependencyInjection.SimpleInjectorBootstraper
{
    public class ContainerBootstraper
    {
        public static void RegisterTypes(Container container)
        {
            var databaseConnectionString
               = ConfigurationManager.ConnectionStrings["MyConnectionString"].ConnectionString;

            IDataAccessComponent dataAccessComponent = new DataAccessComponent(databaseConnectionString);

            container.Register<ILoggingDataSink, LoggingDataSink>(Lifestyle.Transient);
            container.Register<ILoggingComponent>(() => new LoggingComponent(container.GetInstance<ILoggingDataSink>(), "AppLog"));
            container.RegisterSingleton<IDataAccessComponent>(dataAccessComponent);
            container.Register<IWebServiceProxy>(() =>
            {
                string webServiceAddress = ConfigurationManager.AppSettings["MyWebServiceAddress"];

                return new WebServiceProxy(webServiceAddress);
            });
        }
    }
}
