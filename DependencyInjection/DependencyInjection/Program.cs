﻿using System;
using DependencyInjection.SimpleInjectorBootstraper;
using DependencyInjection.UnityBootstrapper;
using Microsoft.Practices.Unity;
using SimpleInjector;
using BusinessService = DependencyInjection.AfterDI.BusinessService;

namespace DependencyInjection
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Without Dependency Injection
            //BusinessService service = new BusinessService();
            #endregion

            #region Using Unity IoC
            UnityContainer ioc = new UnityContainer();
            ContainerBootstrapper.RegisterTypes(ioc);

            var service1 = ioc.Resolve<BusinessService>();
            decimal price = service1.GetPriceById(10);

            Console.WriteLine("Price {0:c}", price);
            //AfterDI.BusinessService service = ioc.Resolve<AfterDI.BusinessService>(new ParameterOverride("logName", "AlternativeAppData"));
            #endregion

            Console.WriteLine("\n\n");

            #region Using SimpleInjector
            var container = new Container();

            ContainerBootstraper.RegisterTypes(container);

            var service2 = container.GetInstance<BusinessService>();
            price = service2.GetPriceById(10);
            #endregion


        }
    }
}
