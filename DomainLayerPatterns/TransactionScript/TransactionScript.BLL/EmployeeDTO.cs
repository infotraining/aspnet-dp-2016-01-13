﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TransactionScript.BLL
{
    public class EmployeeDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int HolidayEntitlement { get; set; }
    }
}
