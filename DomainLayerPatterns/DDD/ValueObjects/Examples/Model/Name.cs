﻿using Examples;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Examples
{
    public class Name : ValueObject<Name>
    {
        public readonly string firstName;
        public readonly string surname;

        public Name(string firstName, string surname)
        {
            Check.that(firstName.is_not_empty()).on_constraint_failure(() =>
            {
                throw new ApplicationException("You must specify a first name.");
            });

            Check.that(surname.is_not_empty()).on_constraint_failure(() =>
            {
                throw new ApplicationException("You must specify a surname.");
            });

            this.firstName = firstName;
            this.surname = surname;
        }

        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new object[] { firstName, surname };
        }
    }

    public static class StringExtensions
    {
        public static bool is_not_empty(this String string_to_check)
        {
            return !String.IsNullOrEmpty(string_to_check);
        }
    }

    public class CheckConstraint
    {
        private readonly bool _assertion;

        public CheckConstraint(bool assertion)
        {
            _assertion = assertion;
        }

        public void on_constraint_failure(Action onFailure)
        {
            if (!_assertion) onFailure();
        }
    }

    public sealed class Check
    {
        public static CheckConstraint that(bool assertion)
        {
            return new CheckConstraint(assertion);
        }
    }
}

