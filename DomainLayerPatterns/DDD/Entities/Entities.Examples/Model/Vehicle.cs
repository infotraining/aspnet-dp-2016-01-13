﻿using System;

namespace Entities.Examples.Model
{
    // Factory used for example - not mandatory
    public static class VehicleFactory
    {
        public static Vehicle CreateVehicle()
        {
            var id = Guid.NewGuid();
            return new Vehicle(id);
        }
    }
    
    public class Vehicle
    {
        public Vehicle(Guid id)
        {
            this.Id = id;
        }

        public Guid Id { get; private set; }
    }
}
