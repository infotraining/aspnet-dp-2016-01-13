﻿namespace DomainServices.Insurance.Model
{
    public interface IPolicyRepository
    {
        Policy Get(int policyId);
    }

}
