﻿using System.Collections.Generic;

namespace DomainServices.Insurance.Model
{
    // Domain Service interface
    public interface IMultiMemberPremiumCalculator
    {
        Quote CalculatePremium(Policy mainPolicy, IEnumerable<Member> additionalMembers);
    }
}
