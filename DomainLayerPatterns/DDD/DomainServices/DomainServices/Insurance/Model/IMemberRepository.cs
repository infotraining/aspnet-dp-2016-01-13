﻿using System.Collections.Generic;

namespace DomainServices.Insurance.Model
{
    public interface IMemberRepository
    {
        IEnumerable<Member> Get(IEnumerable<int> memberIds);
    }
}
