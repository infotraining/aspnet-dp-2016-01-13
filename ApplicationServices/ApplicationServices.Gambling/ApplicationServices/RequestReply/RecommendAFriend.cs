﻿using ApplicationServices.Gambling.ApplicationServices.CommandProcessor;
using ApplicationServices.Gambling.ApplicationServices._3_error_handling;
using NewAccount = ApplicationServices.Gambling.ApplicationServices._4_logging_metrics.NewAccount;

namespace ApplicationServices.Gambling.ApplicationServices.RequestReply
{
    public class RecommendAFriendService
    {
        private IReferralPolicy policy;

        public RecommendAFriendService(Domain.IReferralPolicy policy)
        {
            this.policy = policy;
        }

        public RecommendAFriendResponse RecommendAFriend(RecommendAFriendRequest request)
        {
            try
            {
                var command = new RecommendAFriend
                {
                    ReferrerId = request.ReferrerId,
                    Friend = request.Friend
                };

                policy.Apply(command);

                return new RecommendAFriendResponse
                {
                    Status = RecommendAFriendStatus.Success
                };
            }
            catch (ReferralRejectedDueToLongTermOutstandingBalance)
            {
                return new RecommendAFriendResponse
                {
                    Status = RecommendAFriendStatus.ReferralRejected
                };
            }
        }
    }

    public class RecommendAFriendRequest
    {
        public int ReferrerId { get; set; }

        public NewAccount Friend { get; set; }
    }

    public class RecommendAFriendResponse
    {
        public RecommendAFriendStatus Status { get; set; }
    }

    public enum RecommendAFriendStatus
    {
        Success,
        ReferralRejected
    }
}
