﻿namespace DependencyInjection.Model
{
    public class ChristmasProductDiscount : IProductDiscountStrategy
    {
        public decimal Discount
        {
            get { return 0.1M; }
        }
    }
}
