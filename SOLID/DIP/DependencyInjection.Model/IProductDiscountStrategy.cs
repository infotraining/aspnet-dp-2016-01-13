﻿namespace DependencyInjection.Model
{
    public interface IProductDiscountStrategy
    {
        decimal Discount { get; }
    }
}
