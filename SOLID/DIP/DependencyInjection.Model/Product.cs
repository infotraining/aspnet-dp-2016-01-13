﻿namespace DependencyInjection.Model
{
    public class Product
    {
        public Product(string name, decimal price)
        {
            Name = name;
            Price = price;
        }

        public string Name { get; set; }

        public decimal Price { get; private set; }

        public void AdjustPriceWith(IProductDiscountStrategy discount)
        {
            Price -= Price*discount.Discount;
        }
    }
}
