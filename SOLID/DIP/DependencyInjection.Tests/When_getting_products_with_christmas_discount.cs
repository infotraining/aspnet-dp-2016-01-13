﻿using System;
using System.Collections.Generic;
using System.Linq;
using DependencyInjection.Model;
using FluentAssertions;
using Machine.Specifications;
using Moq;
using It = Machine.Specifications.It;

namespace DependencyInjection.Tests
{
    [Tags("Dependency Inversion Principle")]
    [Subject(typeof(ProductService), "DiscountSpec")]
    public class When_getting_products_with_christmas_discount
    {
        Establish context = () =>
        {
            _mqRepository = new Mock<IProductRepository>();
            _mqRepository.Setup(mq => mq.FindAll())
                .Returns(new List<Product>() {new Product("p1", 10.0M), new Product("p2", 20.0M)});

            _service = new ProductService(_mqRepository.Object);
        };

        Because of = () => { _results = _service.GetProductsWithDiscount(new ChristmasProductDiscount()); };

        It should_return_all_products_with_discount_applied = () =>
        {
            _results.Select(p => p.Name).Should().Equal("p1", "p2");
            _results.Select(p => p.Price).Should().Equal(9, 18);
        };

private static ProductService _service;
        private static Mock<IProductRepository> _mqRepository;
        private static IEnumerable<Product> _results;
    }
}

