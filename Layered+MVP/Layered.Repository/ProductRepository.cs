﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Layered.Model;

namespace Layered.Repository
{
    public class ProductRepository : IProductRepository
    {
        public IList<Model.Product> FindAll()
        {
            using (var context = new ShopEntities())
            {
                var products = from p in context.Products.ToList()
                               select new Model.Product()
                                   {
                                       Id = p.ProductId,
                                       Name = p.ProductName,
                                       Price = new Model.Price(p.RRP, p.SellingPrice)
                                   };

                return products.ToList();
            }            
        }
    }
}
