﻿namespace Layered.Model
{
    public class TradeDiscountStrategy : IDiscountStrategy
    {
        public decimal ApplyExtraDiscountStrategy(decimal originalSalesPrice)
        {
            decimal price = originalSalesPrice * 0.95M;

            return price;
        }
    }
}