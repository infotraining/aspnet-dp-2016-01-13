﻿namespace Layered.Model
{
    public class NullDiscountStrategy : IDiscountStrategy
    {
        public decimal ApplyExtraDiscountStrategy(decimal originalSalesPrice)
        {
            return originalSalesPrice;
        }
    }
}