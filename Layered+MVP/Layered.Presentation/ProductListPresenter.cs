﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Layered.Service;

namespace Layered.Presentation
{
    public class ProductListPresenter
    {
        private IProductListView _productListView;
        private ProductService _productService;

        public ProductListPresenter(IProductListView productListView, ProductService productService)
        {
            _productListView = productListView;
            _productService = productService;
        }

        public void Display()
        {
            ProductListRequest productListRequest = new ProductListRequest()
                {
                    CustomerType = _productListView.CustomerType
                };

            ProductListResponse productListResponse = _productService.GetAllProductsFor(productListRequest);

            if (productListResponse.Success)
            {
                _productListView.Diplay(productListResponse.Products);
            }
            else
            {
                _productListView.ErrorMessage = productListResponse.Message;
            }
        }
    }
}
