﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Layered.Service;

namespace Layered.Presentation
{
    public interface IProductListView
    {
        void Diplay(IList<ProductViewModel> products);
        Model.CustomerType CustomerType { get; }
        string ErrorMessage { set; }
    }
}
