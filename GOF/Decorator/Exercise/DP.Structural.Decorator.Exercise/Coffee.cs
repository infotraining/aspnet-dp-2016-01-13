﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DP.Structural.Decorator.Exercise
{
    public interface ICoffee
    {
        double GetTotalPrice();
        string GetDescription();
        void Prepare();
    }

    /// <summary>
    ///  Coffee
    /// </summary>
    public abstract class Coffee : ICoffee
    {
        protected double price;
        protected string description;

        public virtual double GetTotalPrice()
        {
            return price;
        }

        public virtual string GetDescription()
        {
            return description;
        }

        public abstract void Prepare();
    }

    /// <summary>
    /// Espresso
    /// </summary>
    public class Espresso : Coffee
    {
        public Espresso()
        {
            price = 4.0;
            description = "Espresso";
        }

        public override void Prepare()
        {
            Console.WriteLine("Making a perfect Espresso: 7g, 15bar, 95degC & 25sec");
        }
    }

    /// <summary>
    /// Cappuccino
    /// </summary>
    public class Cappuccino : Coffee
    {
        public Cappuccino()
        {
            price = 6.0;
            description = "Cappuccino";
        }

        public override void Prepare()
        {
            Console.WriteLine("Making a perfect Cappuccino");
        }
    }

    /// <summary>
    /// Latte
    /// </summary>
    public class Latte : Coffee
    {
        public Latte()
        {
            price = 8.0;
            description = "Latte";
        }

        public override void Prepare()
        {
            Console.WriteLine("Making a superb Latte");
        }
    }

    public abstract class CoffeeDecorator : Coffee
    {
        public ICoffee Coffee { get; set; }

        protected CoffeeDecorator(ICoffee coffee, double price, string description)
        {
            Coffee = coffee;
            base.price = price;
            base.description = description;
        }

        protected CoffeeDecorator(double price, string description)
        {
            base.price = price;
            base.description = description;
        }

        protected CoffeeDecorator()
        { }

        public override double GetTotalPrice()
        {
            return base.GetTotalPrice() + Coffee.GetTotalPrice();
        }

        public override string GetDescription()
        {
            return Coffee.GetDescription() + " + " + base.GetDescription();
        }

        public override void Prepare()
        {
            Coffee.Prepare();
        }
    }

    // TO DO: Dodatki: cena - Whipped: 2.5, Whiskey: 6.0, ExtraEspresso: 4.0

    public class Whipped : CoffeeDecorator
    {
        public Whipped(ICoffee coffee) : base(coffee, 2.5, "Whipped Cream")
        {           
        }

        public Whipped()
            : base(2.5, "Whipped Cream")
        {            
        }

        public override void Prepare()
        {
            base.Prepare();

            Console.WriteLine("Adding a whipped cream...");
        }
    }

    public class Whisky : CoffeeDecorator
    {
        public Whisky(ICoffee coffee)
            : base(coffee, 6.0, "Whisky")
        {
        }

        public Whisky()
            : base(6.0, "Whisky")
        {}

        public override void Prepare()
        {
            base.Prepare();

            Console.WriteLine("Pouring 50cl of Jack Daniels...");
        }
    }

    public class ExtraEspresso : CoffeeDecorator
    {
        Espresso _espresso = new Espresso();

        public ExtraEspresso(ICoffee coffee) : base(coffee, 4.0, "Extra espresso")
        {            
        }

        public ExtraEspresso()
            : base(4.0, "Extra espresso")
        {
            
        }

        public override void Prepare()
        {
            base.Prepare();

            _espresso.Prepare();
        }
    }
}
