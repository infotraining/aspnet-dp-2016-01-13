﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DP.Structural.Decorator.Exercise
{
    public class CoffeeBuilder
    {
        private ICoffee _coffee;

        public CoffeeBuilder Create<TBaseCoffee>() where TBaseCoffee : ICoffee, new()
        {
            if (typeof(TBaseCoffee) == typeof(CoffeeDecorator))
                throw new ArgumentException("Decorator can't be base component...");

            _coffee = new TBaseCoffee();

            return this;
        }

        public CoffeeBuilder Add<TCondiment>() where TCondiment : CoffeeDecorator, new()
        {
            var condiment = new TCondiment { Coffee = _coffee };
            _coffee = condiment;

            return this;
        }

        public ICoffee GetCoffee()
        {
            return _coffee;            
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            //Coffee myCoffee = new Whipped(new Whisky(new Whisky(new ExtraEspresso(new  Espresso()))));

            var cb = new CoffeeBuilder();

            cb.Create<Cappuccino>().Add<Whisky>().Add<Whisky>().Add<Whipped>();

            var myCoffee = cb.GetCoffee();

            Console.WriteLine("Coffee: {0} - Price: {1:c}", myCoffee.GetDescription(), myCoffee.GetTotalPrice());
            myCoffee.Prepare();

            Console.ReadKey();
        }
    }
}
