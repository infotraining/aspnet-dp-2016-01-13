﻿namespace DecoratorPattern.Model
{
    public class BasketItem : IBasketItem 
    {        
        public decimal LineTotal
        {
            get  { return Product.Price.Cost * Quantity; }
        }

        public Product Product { get; set; }
        
        public int Quantity { get; set; }
    }
}
