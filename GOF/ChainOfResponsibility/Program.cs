﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DP.Behavioral.ChainOfResposibility.TheoryCode
{
    class MainApp
    {
        /// <summary>
        /// Entry point into console application.
        /// </summary>
        static void Main()
        {
            // Setup Chain of Responsibility
            Handler h1 = new ConcreteHandler1();
            Handler h2 = new ConcreteHandler2();
            Handler h3 = new ConcreteHandler3();
            h1.SetSuccessor(h2);
            h2.SetSuccessor(h3);

            // Generate and process request
            int[] requests = { 2, 5, 14, 22, 18, 3, 27, 20 };

            foreach (int request in requests)
            {
                h1.HandleRequest(request);
            }

            // Wait for user
            Console.ReadKey();
        }
    }

    // "Handler" 

    abstract class Handler
    {
        protected Handler successor;

        public void SetSuccessor(Handler successor)
        {
            this.successor = successor;
        }

        public void HandleRequest(int request)
        {
            if (IsSatisfied(request))
            {
                DoHandleRequest(request);
            }
            else if (successor != null)
            {
                successor.HandleRequest(request);
            }
        }

        protected abstract void DoHandleRequest(int request);

        protected abstract bool IsSatisfied(int request);
        
    }

    // "ConcreteHandler1"

    class ConcreteHandler1 : Handler
    {
        protected override void DoHandleRequest(int request)
        {
            Console.WriteLine("{0} handled request {1}",
                    this.GetType().Name, request);
        }

        protected override bool IsSatisfied(int request)
        {
            return request >= 0 && request < 10;
        }
    }

    // "ConcreteHandler2"

    class ConcreteHandler2 : Handler
    {
        protected override void DoHandleRequest(int request)
        {
            Console.WriteLine("{0} handled request {1}",
                    this.GetType().Name, request);
        }

        protected override bool IsSatisfied(int request)
        {
            return request >= 10 && request < 20;
        }
    }

    // "ConcreteHandler3"

    class ConcreteHandler3 : Handler
    {
        protected override void DoHandleRequest(int request)
        {
            Console.WriteLine("{0} handled request {1}",
                    this.GetType().Name, request);
        }

        protected override bool IsSatisfied(int request)
        {
            return request >= 20 && request < 30;
        }
    }
}
