﻿namespace TemplateMethodPattern.Model
{
    public abstract class ReturnProcessTemplate
    {       
        protected abstract void GenerateReturnTransactionFor(ReturnOrder ReturnOrder);
        protected abstract void CalculateRefundFor(ReturnOrder ReturnOrder);

        public void Process(ReturnOrder ReturnOrder)
        {
            GenerateReturnTransactionFor(ReturnOrder);
            CalculateRefundFor(ReturnOrder);
        }
    }
}
