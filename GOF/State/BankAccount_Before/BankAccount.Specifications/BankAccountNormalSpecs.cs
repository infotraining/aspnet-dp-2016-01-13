using BankAccount.Model;
using Machine.Specifications;

namespace BankAccount.Specifications
{
    public class BankAccountNormalSpecs
    {
        [Behaviors]
        public class BankAccountInNormalState
        {
            It should_return_status_as_normal = () => _account.Status.ShouldEqual(AccountState.Normal);

            protected static Model.BankAccount _account;
        }

        [Subject(typeof(Model.BankAccount), "BankAccount in normal state")]
        public class When_new_account_is_created
        {
            protected static Model.BankAccount _account;

            private Establish context = () => _account = new Model.BankAccount(1);

            private Behaves_like<BankAccountInNormalState> account_in_normal_state;

            private It should_have_initial_balance_zero = () => _account.Balance.ShouldEqual(0.0M);
        }

        public abstract class with_bank_account_in_normal_state
        {
            protected static Model.BankAccount _account;
            protected static decimal _initialBalance = 100.0M;

            protected Establish context = () =>
            {
                _account = new Model.BankAccount(1);
                _account.Deposit(_initialBalance);
            };
        }

        public class When_making_deposit_in_normal_state : with_bank_account_in_normal_state
        {
            private Because of = () => _account.Deposit(100.0M);

            private Behaves_like<BankAccountInNormalState> account_in_normal_state;

            private It should_update_the_balance = () => _account.Balance.ShouldEqual(200.0M);
        }

        [Subject(typeof(Model.BankAccount), "BankAccount in normal state")]
        public class When_making_withdrawal_less_than_balance : with_bank_account_in_normal_state
        {
            private Because of = () => _account.Withdraw(99.0M);

            private Behaves_like<BankAccountInNormalState> account_in_normal_state;

            private It should_update_the_balance = () => _account.Balance.ShouldEqual(1.0M);
        }

        [Subject(typeof(Model.BankAccount), "BankAccount in normal state")]
        public class When_making_withdrawal_greater_than_balance : with_bank_account_in_normal_state
        {
            private Because of = () => _account.Withdraw(200.0M);

            private It should_subtract_proper_amount_from_balance = () => _account.Balance.ShouldEqual(-100.0M);

            private It should_change_status_to_overdraft = () => _account.Status.ShouldEqual(AccountState.Overdraft);
        }

        [Subject(typeof(Model.BankAccount), "BankAccount in normal state")]
        public class When_paying_interests : with_bank_account_in_normal_state
        {
            private Because of = () => _account.PayInterest();

            private Behaves_like<BankAccountInNormalState> account_in_normal_state; 

            private It should_increase_balance_with_interests = () => _account.Balance.ShouldEqual(110.0M);
        }
    }
}