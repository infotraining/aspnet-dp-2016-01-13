﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Machine.Specifications;

namespace MSpec.Demo
{
    //internal class MyTestFixture
    //{
    //    [Test]
    //    private void XXX()
    //    {
    //        // Arrange
    //        var list = new List<int>();
    //        private
    //        int prevSize = list.Count;

    //        // Act
    //        list.Add(1);

    //        // Assert
    //        Assert.Equals(list.Count, prevSize + 1);
    //    }

    //    private void XXX()
    //    {
    //        // Arrange
    //        var list = new List<int>();
    //        private
    //        int prevSize = list.Count;

    //        // Act
    //        list.Add(1);

    //        // Assert
    //        Assert.Equals(list.Last(), 1);
    //    }
    //}

    [Subject(typeof (List<int>))]
    class When_adding_an_item_to_list
    {
        protected static List<int> _list;
        protected static int _prevCount;

        Establish context = () =>
        {
            _list = new List<int>();
            _prevCount = _list.Count;
        };

        Because of = () => { _list.Add(1); };

        private It should_increase_size = () => _list.Count.ShouldEqual(_prevCount + 1);
        private It should_store_added_item_at_the_end = () => _list.Last().ShouldEqual(1);
    }
}
