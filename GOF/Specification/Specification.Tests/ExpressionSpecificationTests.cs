﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Specification.Model;
using SpecificationPattern;

namespace Specification.Tests
{
    [TestFixture]
    public class ExpressionSpecificationTests
    {
        private ISpecification<CustomerAccount> _customerAccountHasLateFees;

        [SetUp]
        public void Setup()
        {
            _customerAccountHasLateFees = new ExpressionSpecification<CustomerAccount>(ca => ca.LateFees > 0);
        }

        [Test]
        public void CustomerAccountHasLateFeesSpecification_Should_Be_Satisfied_By_A_Customer_With_Late_Fees()
        {
            CustomerAccount customerWithLateFess = new CustomerAccount() { LateFees = 100 };

            Assert.IsTrue(_customerAccountHasLateFees.IsSatisfiedBy(customerWithLateFess));
        }

        [Test]
        public void CustomerAccountHasLateFeesSpecification_Should_Not_Be_Satisfied_By_A_Customer_With_Zero_Late_Fees()
        {
            CustomerAccount customerWithNoLateFess = new CustomerAccount() { LateFees = 0 };

            Assert.IsFalse(_customerAccountHasLateFees.IsSatisfiedBy(customerWithNoLateFess));
        }

        [Test]
        public void CustomerAccountHasLateFeesSpecification_Should_Not_Be_Satisfied_By_A_Customer_With_Late_Fees_When_Used_With_A_NotSpecification()
        {
            CustomerAccount customerWithLateFess = new CustomerAccount() { LateFees = 100 };

            Assert.IsFalse(_customerAccountHasLateFees.Not().IsSatisfiedBy(customerWithLateFess));
        }

    }
}
