﻿using SpecificationPattern;

namespace Specification.Model
{
    public class CustomerAccountHasLateFeesSpecification : CompositeSpecification<CustomerAccount>  
    {
        public override bool IsSatisfiedBy(CustomerAccount candidate)
        {
            return candidate.LateFees > 0;
        }
    }
}
