﻿using SpecificationPattern;

namespace Specification.Model
{
    public class HasReachedRentalThresholdSpecification : CompositeSpecification<CustomerAccount> 
    {
        public override bool IsSatisfiedBy(CustomerAccount candidate)
        {       
            return candidate.NumberOfRentalsThisMonth >= 5;        
        }
    }
}
