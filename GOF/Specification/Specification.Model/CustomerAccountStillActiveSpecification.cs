﻿using SpecificationPattern;

namespace Specification.Model
{
    public class CustomerAccountStillActiveSpecification : CompositeSpecification<CustomerAccount>  
    {
        public override bool IsSatisfiedBy(CustomerAccount candidate)
        {
            return candidate.AccountActive;
        }
    }
}
