namespace FactoryPattern.Model.Creators
{
    public interface ICourierCreator
    {
        IShippingCourier CreateShippingCourier();
    }
}