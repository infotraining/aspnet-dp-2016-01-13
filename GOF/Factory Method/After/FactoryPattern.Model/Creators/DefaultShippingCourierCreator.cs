namespace FactoryPattern.Model.Creators
{
    public class DefaultShippingCourierCreator : ICourierCreator
    {
        public IShippingCourier CreateShippingCourier()
        {
            return new RoyalMail();
        }
    }
}