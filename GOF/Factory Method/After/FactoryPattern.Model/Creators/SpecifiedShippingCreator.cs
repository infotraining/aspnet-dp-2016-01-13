using SpecificationPattern;

namespace FactoryPattern.Model.Creators
{
    public class SpecifiedShippingCreator
    {
        public ISpecification<Order> Specification { get; set; }
        public ICourierCreator GetShippingCreator { get; set; }
    }
}