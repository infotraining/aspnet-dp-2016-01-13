﻿using System.Collections;
using System.Collections.Generic;
using FactoryPattern.Model.Creators;

namespace FactoryPattern.Model
{
    public class UKShippingCourierFactory : IShippingCourierFactory
    {
        private IList<SpecifiedShippingCreator> _creators = new List<SpecifiedShippingCreator>();
        private ICourierCreator _defaultCourierCreator;

        public UKShippingCourierFactory(ICourierCreator defaultCourierCreator)
        {
            _defaultCourierCreator = defaultCourierCreator;
        }

        public IShippingCourier CreateShippingCourier(Order order)
        {
            var shippingCourier = FindSpecifiedShippingCourier(order);

            if (shippingCourier == null)
                shippingCourier = _defaultCourierCreator.CreateShippingCourier();

            return shippingCourier;
        }

        private IShippingCourier FindSpecifiedShippingCourier(Order order)
        {
            IShippingCourier shippingCourier = null;

            foreach (var specifiedCreator in _creators)
            {
                if (specifiedCreator.Specification.IsSatisfiedBy(order))
                    shippingCourier = specifiedCreator.GetShippingCreator.CreateShippingCourier();
            }

            return shippingCourier;
        }

        public void RegisterSpecifiedCreator(SpecifiedShippingCreator specifiedCreator)
        {
            _creators.Add(specifiedCreator);
        }
    }
}
