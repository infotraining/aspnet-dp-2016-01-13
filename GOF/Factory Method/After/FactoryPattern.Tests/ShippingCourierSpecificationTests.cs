﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FactoryPattern.Model;
using FactoryPattern.Model.Specifications;
using NUnit.Framework;
using NUnit.Framework.SyntaxHelpers;

namespace FactoryPattern.Tests
{
    [TestFixture]
    public class ShippingCourierSpecificationTests
    {
        [Test]
        public void WeightOverFiveKGSpecification_Is_Satisfied_When_Weight_Exceeds_5_KG()
        {
            var order = new Order {WeightInKG = 5.5M};

            var weightSpec = new WeightOver5KGSpecification();

            Assert.That(weightSpec.IsSatisfiedBy(order), Is.True);
        }

        [Test]
        public void WeightOverFiveKGSpecification_Is_Not_Satisfied_When_Weight_Is_Below_5KG()
        {
            var order = new Order { WeightInKG = 4.5M };

            var weightSpec = new WeightOver5KGSpecification();

            Assert.That(weightSpec.IsSatisfiedBy(order), Is.False);
        }

        [Test]
        public void TotalCostSpecification_Is_Satisfied_When_Order_TotalCost_Exceeds_100()
        {
            var order = new Order() {TotalCost = 101.9M};

            var totalCostSpec = new TotalCostSpecification();

            Assert.That(totalCostSpec.IsSatisfiedBy(order), Is.True);
        }

        [Test]
        public void TotalCostSpecification_Is_Satisfied_When_Order_TotalCost_Is_Less_Than_100()
        {
            var order = new Order() { TotalCost = 99.9M };

            var totalCostSpec = new TotalCostSpecification();

            Assert.That(totalCostSpec.IsSatisfiedBy(order), Is.False);
        }
    }
}
