﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Stategy.DataAnalyzer.Before;
using Strategy.DataAnalyzer;

namespace Stategy.DataAnalyzer.Before
{
    internal enum StatType
    {
        Avg,
        Sum,
        StdDev
    };

    interface IStatistics
    {
        StatResult Calc(IEnumerable<double> data);
    }

    class Avg : IStatistics
    {
        public StatResult Calc(IEnumerable<double> data)
        {
            return new StatResult() { Name = "Avg", Value = data.Average() };
        }
    }

    class StdDev : IStatistics
    {
        public StatResult Calc(IEnumerable<double> data)
        {
            return new StatResult() { Name = "StdDev", Value = 3.212 };
        }
    }

    class Sum : IStatistics
    {
        public StatResult Calc(IEnumerable<double> data)
        {
            return new StatResult() { Name = "Avg", Value = data.Sum() };
        }
    }


    class DataAnalyzer
    {
        private IEnumerable<double> _data;

        public IStatistics StatType { get; set; }

        public void LoadData()
        {
            _data = new List<double>() { 1, 2, 3, 4, 5, 6 };
        }

        public StatResult Calculate()
        {
            return StatType.Calc(_data);            
        }
    }
}

namespace Strategy.DataAnalyzer
{

    class StatResult
    {
        public string Name { get; set; }
        public double Value { get; set; }
    }



    class Program
    {
        static void Main(string[] args)
        {
            Stategy.DataAnalyzer.Before.DataAnalyzer da = new Stategy.DataAnalyzer.Before.DataAnalyzer();

            da.StatType = new Avg();

            da.LoadData();

            var results = new List<StatResult>();

            results.Add(da.Calculate());

            da.StatType = new StdDev();

            results.Add(da.Calculate());

            da.StatType = new Sum();

            results.Add(da.Calculate());
        }
    }
}
