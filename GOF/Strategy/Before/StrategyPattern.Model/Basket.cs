﻿namespace StrategyPattern.Model
{
    public class Basket
    {
        private DiscountType _discountType;

        public Basket(DiscountType discountType)
        {
            _discountType = discountType;
        }

        public decimal TotalCost { get; set; }        

        public decimal GetTotalCostAfterDiscount()
        {
            if (_discountType == DiscountType.MoneyOff)
            {
                if (TotalCost > 100)
                    return TotalCost - 10m;
                if (TotalCost > 50)
                    return TotalCost - 5m;
                else
                    return TotalCost;
            }
            else if (_discountType == DiscountType.PercentageOff)
            {
                return TotalCost * 0.85m;
            }
            else
            {
                return TotalCost;
            }
        }
    }
}
