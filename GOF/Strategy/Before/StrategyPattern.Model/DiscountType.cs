﻿namespace StrategyPattern.Model
{
    public enum DiscountType
    {
        NoDiscount = 0,
        MoneyOff = 1,
        PercentageOff = 2
    }
}
